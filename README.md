Project for testing CI in GitLab

**Tasks:**
1. Create environment with Python 3.8 and Selenium Grid.
2. Install library from requirements.
3. Launch tests for http://way2automation.com.

p.s.: time.sleep() used just for visibility of precesses in tests
