class TestUrl:
    DRAG_URL = "http://way2automation.com/way2auto_jquery/droppable.php#load_box"
    SORT_URL = "http://way2automation.com/way2auto_jquery/sortable.php"
    AUTHORISATION_URL = "http://way2automation.com/way2auto_jquery/index.php"
    MAIN_PAGE_COOKIES = "http://way2automation.com/way2auto_jquery/automation-practice-site.html"
    IFRAME_URL = "http://way2automation.com/way2auto_jquery/menu.php#load_box"
    DRAG_URL_SORT = "http://way2automation.com/way2auto_jquery/sortable.php#load_box"
    PAGE_WINDOWS = "http://way2automation.com/way2auto_jquery/frames-and-windows.php"
    ALERT_PAGE = "http://way2automation.com/way2auto_jquery/alert.php#load_box"
    BASIC_AUTH_PAGE = "https://httpwatch:httpwatch@www.httpwatch.com/httpgallery/authentication/#showExample10"


class NewUser:
    NAME = "Viktor"
    PHONE = "+79010000000"
    EMAIL = "viktor@mail.ru"
    CITY = "Moscow"
    USERNAME = "viggo"
    PASSWORD = "123"


class DataFromPages:
    TEXT_FIRST_ROW_SUBMENU = "Sub Menu 1"
    TEXT_MAIN_PAGE = "Try Automating all feasible elements you find on web"
    TEXT_ALERT = "This is just a dummy form, you just clicked SUBMIT BUTTON"
    TEXT_FIRST_ELEMENT_SORTED = "Item 7"
    TEXT_LAST_ELEMENT_SORTED = "Item 1"
    VALUE_ALERT = "Vlad Sazhatel"
    LOGIN_BASIC_AUTH = "httpwatch"
    PASS_BASIC_AUTH = "httpwatch"
