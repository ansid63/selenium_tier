import allure
from selenium.webdriver.common.by import By
from page_objects.base_page import BasePageObject
from data.test_data import TestUrl, DataFromPages
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait as Wait


class BasicAuthPage(BasePageObject):

    def __init__(self, driver):
        super(BasicAuthPage, self).__init__(driver)
        self._button_display_image = (By.XPATH, "//input[@id='displayImage']")
        self._image = (By.XPATH, "//img[@id='downloadImg']")

    @allure.step("Переход на страницу авторизации")
    def get_auth_page(self):
        self._driver.get(TestUrl.BASIC_AUTH_PAGE)

    @allure.step("Нажать на кнопку Display Image")
    def press_button_display_image(self):
        self._driver.find_element(*self._button_display_image).click()

    @allure.step("Ввести данные авторизации")
    def image_after_auth_enabled(self):
        image_enabled = Wait(self._driver, 5).until(EC.visibility_of_element_located(self._image)).is_enabled()
        return image_enabled

