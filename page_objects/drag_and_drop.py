import allure
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait as Wait
from selenium.webdriver.common.action_chains import ActionChains

from data.test_data import TestUrl
from page_objects.base_page import BasePageObject


class DragAndDrop(BasePageObject):

    def __init__(self, driver):
        super(DragAndDrop, self).__init__(driver)
        self._drag = (By.XPATH, "//div[@id = 'draggable']")
        self._drop = (By.XPATH, "//div[@id = 'droppable']")
        self._drop_text = (By.XPATH, "//div[@id = 'droppable']/p")

    @allure.step("Get URL")
    def get_page(self):
        self._driver.get(TestUrl.DRAG_URL)

    @allure.step("Drag and Drop element 1 to 2 position")
    def drag_and_drop(self):
        self._driver.switch_to.frame(self._driver.find_element_by_tag_name("iframe"))
        element_drag = Wait(self._driver, 5).until(EC.element_to_be_clickable(self._drag))
        element_drop = Wait(self._driver, 5).until(EC.element_to_be_clickable(self._drop))
        ActionChains(self._driver).drag_and_drop(element_drag, element_drop).perform()

    @allure.step("Get text from droppable element")
    def get_text_from_droppable(self):
        text = self._driver.find_element(*self._drop_text).text
        return text

    @allure.step("Scroll to the center of the page")
    def js_scroll(self):
        self._driver.switch_to.default_content()
        self._driver.execute_script("window.scrollTo(0, 600);")

    @allure.step("Focus on element scroll up")
    def js_focus_on_element(self):
        self._driver.execute_script("document.getElementById('toTop').focus();")

    @allure.step("Press element and scroll up")
    def js_click_on_element(self):
        self._driver.execute_script("document.getElementById('toTop').click();")
