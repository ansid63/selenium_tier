import allure
import pickle
from selenium.webdriver.common.by import By
from page_objects.base_page import BasePageObject
from data.test_data import TestUrl, NewUser
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class LoginPage(BasePageObject):

    def __init__(self, driver):
        super(LoginPage, self).__init__(driver)
        self._name_field = (By.XPATH, "//input[@name='name']")
        self._phone_field = (By.XPATH, "//input[@name='phone']")
        self._email_field = (By.XPATH, "//input[@name='email']")
        self._country_field = (By.XPATH, "//option[@value='Bolivia']")
        self._city_field = (By.XPATH, "//input[@name='city']")
        self._username_field = (By.XPATH, "//fieldset[6]//input")
        self._password_field = (By.XPATH, "//fieldset[7]//input")
        self._button_submit = (By.XPATH, "//div[@class='fancybox-skin']//input[@value='Submit']")
        self._alert_authorisation = (By.XPATH, "//p[@id='alert']")
        self._text_main_page = (By.XPATH, "//h3[@class='iternal_h3']")

    @allure.step("Авторизация нового пользователя и сохранение Cookies")
    def authorization_and_save_cookies(self):
        self._driver.get(TestUrl.AUTHORISATION_URL)
        self._driver.find_element(*self._name_field).send_keys(NewUser.NAME)
        self._driver.find_element(*self._phone_field).send_keys(NewUser.PHONE)
        self._driver.find_element(*self._email_field).send_keys(NewUser.EMAIL)
        self._driver.find_element(*self._country_field).click()
        self._driver.find_element(*self._city_field).send_keys(NewUser.CITY)
        self._driver.find_element(*self._username_field).send_keys(NewUser.USERNAME)
        self._driver.find_element(*self._password_field).send_keys(NewUser.PASSWORD)
        self._driver.find_element(*self._button_submit).click()
        cookies = self._driver.get_cookies()
        with open("cookies.pkl", "wb") as f:
            pickle.dump(cookies, f)

    @allure.step("Возвращает тект оповещения после авторизации")
    def get_text_after_authorisation(self):
        text_alert = \
            WebDriverWait(self._driver, 7).until(EC.visibility_of_element_located(self._alert_authorisation)).text
        return text_alert

    @allure.step("Переход на основную страницу сайта с сохраненными Cookies")
    def move_to_page_with_saved_cookies(self):

        with open("cookies.pkl", "rb") as f:
            self._driver.get(TestUrl.MAIN_PAGE_COOKIES)
            cookies = pickle.load(open("cookies.pkl", "rb"))
            for cookie in cookies:
                self._driver.add_cookie(cookie)

    @allure.step("Проверяем наличие элемента на странице")
    def get_text_on_main_page(self):
        text_line = self._driver.find_element(*self._text_main_page).text
        return text_line

    @allure.step("Загрузка формы авторизации, наведение фокуса на поле Имя")
    def get_authorisation_page_click_name_field(self):
        self._driver.get(TestUrl.AUTHORISATION_URL)
        self._driver.find_element(*self._name_field).click()

    @allure.step("Меняем фокус черех JSE")
    def step_change_focus_by_jse(self):
        self._driver.execute_script("document.getElementById('link').focus();")

    @allure.step("Загрузка главной страницы")
    def get_main_page(self):
        self._driver.get(TestUrl.MAIN_PAGE_COOKIES)

    @allure.step("Меняем фокус черех JSE")
    def step_scroll_by_jse(self):
        self._driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
