import allure
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait as Wait
from data.test_data import TestUrl, DataFromPages
from page_objects.base_page import BasePageObject


class AlertPage(BasePageObject):

    def __init__(self, driver):
        super(AlertPage, self).__init__(driver)
        self._input_alert_button = (By.XPATH, "//ul[@class='responsive-tabs']/li[2]")
        self._frame = (By.XPATH, "//iframe[@src='alert/input-alert.html']")
        self._raise_alert_button = (By.XPATH, "//body/button")
        self._text_created = (By.XPATH, "//p[@id='demo']")

    @allure.step("Перейти на страницу Alert")
    def get_page(self):
        self._driver.get(TestUrl.ALERT_PAGE)

    @allure.step("Выбрать в меню диалоговое окно")
    def choose_input_alert(self):
        self._driver.find_element(*self._input_alert_button).click()

    @allure.step("Нажать кнопку вызова диалогового окна")
    def press_raise_alert(self):
        self._driver.switch_to.frame(self._driver.find_element(*self._frame))
        self._driver.find_element(*self._raise_alert_button).click()

    @allure.step("Ввести кастомный текст в окне и нажать кнопку Ок")
    def insert_text_ana_press_ok(self):
        Wait(self._driver, 3).until(EC.alert_is_present(), 'Timed out waiting for popup to appear.')
        alert = self._driver.switch_to.alert
        alert.send_keys(DataFromPages.VALUE_ALERT)
        alert.accept()

    @allure.step("Вернуть созданный текст из формы")
    def get_created_text(self):
        text_from_form = self._driver.find_element(*self._text_created).text
        return text_from_form
