import allure
from selenium.webdriver.common.by import By
from page_objects.base_page import BasePageObject
from data.test_data import TestUrl


class PageTab(BasePageObject):

    def __init__(self, driver):
        super(PageTab, self).__init__(driver)
        self._link = (By.XPATH, "//div[@class='farme_window'][1]//a[@target='_blank']")
        self._frame = (By.XPATH, "//iframe[@src='frames-windows/defult1.html']")

    @allure.step("На странице задания перейти по ссылке переключиться на второе окно, "
                 "перейти по ссылке переключиться на третье окно")
    def tab_windows(self):
        self._driver.get(TestUrl.PAGE_WINDOWS)
        self._driver.switch_to.frame(self._driver.find_element(*self._frame))
        self._driver.find_element(*self._link).click()
        self._driver.switch_to.window(self._driver.window_handles[1])
        self._driver.find_element(*self._link).click()
        self._driver.switch_to.window(self._driver.window_handles[2])

    @allure.step("Проверить что третье окнооткрылось и содержит ссылку")
    def link_third_windows_enabled(self):
        link_enabled = self._driver.find_element(*self._link).is_enabled()
        return link_enabled
