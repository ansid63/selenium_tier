import allure
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait as Wait
from selenium.webdriver.common.action_chains import ActionChains

from data.test_data import TestUrl
from page_objects.base_page import BasePageObject


class DragAndDropSort(BasePageObject):

    def __init__(self, driver):
        super(DragAndDropSort, self).__init__(driver)
        self._drag_7 = (By.XPATH, "//li[contains(text(),'Item 7')]")
        self._drag_6 = (By.XPATH, "//li[contains(text(),'Item 6')]")
        self._drag_5 = (By.XPATH, "//li[contains(text(),'Item 5')]")
        self._drag_4 = (By.XPATH, "//li[contains(text(),'Item 4')]")
        self._drag_3 = (By.XPATH, "//li[contains(text(),'Item 3')]")
        self._drag_2 = (By.XPATH, "//li[contains(text(),'Item 2')]")
        self._drop_1 = (By.XPATH, "//li[contains(text(),'Item 1')]")

        self._1_element_sorted = (By.XPATH, "//body/ul/li[1]")
        self._7_element_sorted = (By.XPATH, "//body/ul/li[7]")

    @allure.step("Перейти на страницу сортировки")
    def get_page(self):
        self._driver.get(TestUrl.DRAG_URL_SORT)

    @allure.step("Drag and Drop элементы сортируя по убыванию")
    def drag_and_drop(self):
        self._driver.switch_to.frame(self._driver.find_element_by_tag_name("iframe"))
        element_drag_7 = Wait(self._driver, 5).until(EC.element_to_be_clickable(self._drag_7))
        element_drop_1 = Wait(self._driver, 5).until(EC.element_to_be_clickable(self._drop_1))
        ActionChains(self._driver).drag_and_drop(element_drag_7, element_drop_1).perform()
        element_drag_6 = self._driver.find_element(*self._drag_6)
        ActionChains(self._driver).drag_and_drop(element_drag_6, element_drop_1).perform()
        element_drag_5 = self._driver.find_element(*self._drag_5)
        ActionChains(self._driver).drag_and_drop(element_drag_5, element_drop_1).perform()
        element_drag_4 = self._driver.find_element(*self._drag_4)
        ActionChains(self._driver).drag_and_drop(element_drag_4, element_drop_1).perform()
        element_drag_3 = self._driver.find_element(*self._drag_3)
        ActionChains(self._driver).drag_and_drop(element_drag_3, element_drop_1).perform()
        element_drag_2 = self._driver.find_element(*self._drag_2)
        ActionChains(self._driver).drag_and_drop(element_drag_2, element_drop_1).perform()

    @allure.step("Проверка правильности расположения элементов")
    def get_text_from_sorted_list(self):
        text_1_element = self._driver.find_element(*self._1_element_sorted).text
        text_7_element = self._driver.find_element(*self._7_element_sorted).text
        first_and_last_text_in_list = (text_1_element, text_7_element)
        return first_and_last_text_in_list
