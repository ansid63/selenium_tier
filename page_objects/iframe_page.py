import allure
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait as Wait
from selenium.webdriver.common.action_chains import ActionChains
from data.test_data import TestUrl
from page_objects.base_page import BasePageObject


class IFrameAndMousePage(BasePageObject):

    def __init__(self, driver):
        super(IFrameAndMousePage, self).__init__(driver)
        self._menu_with_submenu = (By.XPATH, "//*[contains(text(),'Menu With Sub Menu')]")
        self._frame = (By.XPATH, "//iframe[@src='menu/defult2.html']")
        self._submenu_delphi_button = (By.XPATH, "//*[@id='ui-id-8' and @aria-haspopup='true']")
        self._second_submenu_first_button = (By.XPATH, "//*[@id='ui-id-9']")

    @allure.step("Перейти на страницу iFrame")
    def get_page(self):
        self._driver.get(TestUrl.IFRAME_URL)

    @allure.step("Выбрать меню с сабменю")
    def choose_menu_with_submenu(self):
        self._driver.find_element(*self._menu_with_submenu).click()

    @allure.step("Перекрытие субменю мышью")
    def choose_first_level_submenu(self):
        self._driver.switch_to.frame(self._driver.find_element(*self._frame))
        first_level_submenu = self._driver.find_element(*self._submenu_delphi_button)
        action = ActionChains(self._driver)
        action.move_to_element(first_level_submenu).perform()

    @allure.step("Проверка наличия субменю второго уровня")
    def get_second_level_submenu_text(self):
        second_level_submenu = \
            Wait(self._driver, 10).until(EC.element_to_be_clickable(self._second_submenu_first_button)).text
        return second_level_submenu
