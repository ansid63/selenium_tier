import allure
from page_objects.alert_page import AlertPage
from data.test_data import DataFromPages


class TestIFrameAndMouse:

    @allure.title("Выбрать подменю в меню")
    def test_alert(self, browser):
        alert_page = AlertPage(browser)
        alert_page.get_page()
        alert_page.choose_input_alert()
        alert_page.press_raise_alert()
        alert_page.insert_text_ana_press_ok()
        text_created = alert_page.get_created_text()
        assert DataFromPages.VALUE_ALERT in text_created, "Текст из диалогового окна не появился в форме"
