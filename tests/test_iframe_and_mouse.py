import allure
from page_objects.iframe_page import IFrameAndMousePage
from data.test_data import DataFromPages


class TestIFrameAndMouse:

    @allure.title("Выбрать подменю в меню")
    def test_iframe_and_mouse_focus(self, browser):
        iframe_page = IFrameAndMousePage(browser)
        iframe_page.get_page()
        iframe_page.choose_menu_with_submenu()
        iframe_page.choose_first_level_submenu()
        text_first_row_second_submenu = iframe_page.get_second_level_submenu_text()
        assert text_first_row_second_submenu == DataFromPages.TEXT_FIRST_ROW_SUBMENU, "Неверный текст сабменю"
