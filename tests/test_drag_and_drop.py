import time
import allure

from page_objects.drag_and_drop import DragAndDrop


class TestDragAndDrop:

    @allure.title("Drag and drop слева на право")
    def test_drag_and_drop_basic(self, browser):
        action_page = DragAndDrop(browser)
        action_page.get_page()
        action_page.drag_and_drop()
        text_second_element = action_page.get_text_from_droppable()
        assert text_second_element == "Dropped!"
        action_page.js_scroll()
        time.sleep(1)
        action_page.js_focus_on_element()
        action_page.js_click_on_element()
        time.sleep(3)
