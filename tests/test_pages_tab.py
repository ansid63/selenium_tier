import allure
from page_objects.pages_tab import PageTab


class TestPagesTab:

    @allure.title("Open 2 tabs, move between tabs to first one")
    def test_tab(self, browser):
        tab_page = PageTab(browser)
        tab_page.tab_windows()
        link_third_page_enabled = tab_page.link_third_windows_enabled()
        assert link_third_page_enabled, "Третье окно не открылось"
