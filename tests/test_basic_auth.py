import allure
from page_objects.basic_auth_page import BasicAuthPage


class TestBasicAuth:

    @allure.severity(allure.severity_level.CRITICAL)
    @allure.title("Авторизация с помощью Basic Auth")
    def test_basic_auth(self, browser):
        auth_page = BasicAuthPage(browser)
        auth_page.get_auth_page()
        auth_page.press_button_display_image()
        image_after_auth_enabled = auth_page.image_after_auth_enabled()
        assert image_after_auth_enabled, "Авторизация не прошла успешно"
