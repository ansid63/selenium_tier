import allure
from page_objects.login_page import LoginPage
from data.test_data import DataFromPages


class TestReuseCookiesAfterAuthorisation:

    @allure.title("Авторизация с сохранением Cookies, вход с Cookies на основную страницу")
    def test_save_cookies(self, browser):
        login_page = LoginPage(browser)
        login_page.authorization_and_save_cookies()
        text_alert = login_page.get_text_after_authorisation()
        assert text_alert == DataFromPages.TEXT_ALERT, "Авторизация не прошла"

    @allure.title("Авторизация с сохранением Cookies, вход с Cookies на основную страницу")
    def test_add_cookies_to_new_session(self, browser):
        login_page = LoginPage(browser)
        login_page.move_to_page_with_saved_cookies()
        text_main_page = login_page.get_text_on_main_page()
        assert text_main_page == DataFromPages.TEXT_MAIN_PAGE, "Элемент на основной странице не загрузился"
