import allure
from page_objects.drag_and_drop_sort import DragAndDropSort
from data.test_data import DataFromPages


class TestDragAndDrop:

    @allure.title("Drag and drop сортировка по убыванию")
    def test_drag_and_drop_sorted(self, browser):
        action_page = DragAndDropSort(browser)
        action_page.get_page()
        action_page.drag_and_drop()
        text_first_element, text_last_element = action_page.get_text_from_sorted_list()
        assert text_first_element == DataFromPages.TEXT_FIRST_ELEMENT_SORTED, "Список не отсортирован"
        assert text_last_element == DataFromPages.TEXT_LAST_ELEMENT_SORTED, "Список не отсортирован"
