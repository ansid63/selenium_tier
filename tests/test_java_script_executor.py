import allure
from page_objects.login_page import LoginPage
import time


class TestJavaScriptExecutor:

    @allure.title("Тест JavaScriptExecutor смена фокуса")
    def test_java_script_executor_change_focus(self, browser):
        login_page = LoginPage(browser)
        login_page.get_authorisation_page_click_name_field()
        time.sleep(1)
        login_page.step_change_focus_by_jse()
        time.sleep(1)

    @allure.title("Тест JavaScriptExecutor скролл")
    def test_java_script_executor_scroll(self, browser):
        login_page = LoginPage(browser)
        login_page.get_main_page()
        login_page.step_scroll_by_jse()
        time.sleep(2)
