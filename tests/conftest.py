import pytest
import allure
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.chrome.options import Options


@pytest.hookimpl(hookwrapper=True, tryfirst=True)
def pytest_runtest_makereport(item):
    outcome = yield
    rep = outcome.get_result()
    setattr(item, "rep_" + rep.when, rep)
    return rep


def pytest_addoption(parser):
    parser.addoption("--baseurl", action="store", default="localhub",
                     help="Choose hub URL. Default is http://localhost:4444/wd/hub")


@pytest.fixture(scope="function")
def browser(request):
    _driver = None
    obtained_base_url = request.config.getoption("--baseurl")
    if obtained_base_url == "localhub":
        _driver = webdriver.Remote(command_executor="http://localhost:4444/wd/hub",
                                   desired_capabilities={"browserName": "chrome"})
    elif obtained_base_url == "gitlabhub":
        chrome_options = Options()
        chrome_options.add_argument("--no-sandbox")
        # chrome_options.add_argument("--headless")
        _driver = webdriver.Remote(command_executor="http://selenium__standalone-chrome:4444/wd/hub",
                                   desired_capabilities=DesiredCapabilities.CHROME,
                                   options=chrome_options)

    _driver.maximize_window()
    yield _driver
    if request.node.rep_call.failed:
        try:
            allure.attach(_driver.get_screenshot_as_png(),
                          name=request.function.__name__,
                          attachment_type=allure.attachment_type.PNG)
        except:
            pass
    _driver.quit()
